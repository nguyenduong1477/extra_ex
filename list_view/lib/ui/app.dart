import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "list view",
      home:RandomWord()
    );
  }
}

class RandomWord extends StatefulWidget{
  @override
  State<StatefulWidget> createState(){
    return RandomWordState();
  }
}

class RandomWordState extends State<StatefulWidget>{
  final List<WordPair> _word = <WordPair>[];
  final _biggerfont = const TextStyle(fontSize: 18.0);
  final Set<WordPair> _saved = new Set<WordPair>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My List View"),
        actions: <Widget>[
          new IconButton(icon: const Icon(Icons.list),onPressed: _pushSaved,)
        ],
      ),
      body: Center(
        child: ListView.builder(
          itemBuilder: (context, index){
    if(index.isOdd){
    return Divider();
    }
    if(index>=_word.length){
    _word.addAll(generateWordPairs().take(10));
    }
    return _builRow(_word[index]);
    }
    )
      ),
    );

  }
  Widget _builRow(WordPair wordPair){
    final bool alreadySaved = _saved.contains(wordPair);
    return ListTile(
      title: Text(
          wordPair.asPascalCase, 
          style: _biggerfont
      ),
      trailing: new Icon(
          alreadySaved? Icons.favorite : Icons.favorite_border,
          color: alreadySaved? Colors.yellow : null,
      ),
      onTap: (){
        setState(() {
          if(alreadySaved){
            _saved.remove(wordPair);
          }
          else{
            _saved.add(wordPair);
          }
        });
      },
    );
  }
  void _pushSaved(){
    Navigator.of(context).push(
     new MaterialPageRoute(builder: (BuildContext context) {
       final Iterable<ListTile> tiles = _saved.map((WordPair pair){
         return new ListTile(
           title: new Text(
             pair.asPascalCase,
             style: _biggerfont,
           ),
         );
       });
       final List<Widget> divided = ListTile.divideTiles(tiles: tiles,context: context).toList();
       return new Scaffold(
         appBar: new AppBar(
           title: Text("saved list"),
         ),
         body: new ListView(children: divided,),
       );
     })
    );

  }
}

